import React, { Component } from 'react';
import Channel from "./channel"

class Channels extends Component {
    render() { 
        let channels = this.props.channels.map(channel => {
            return <Channel key={channel.id} currentChannel={this.props.currentChannel} channel={channel} onChannelSelect={this.props.onChannelSelect}/>
        })
        return ( 
            <div className="list-group channel-list">
                {channels}
            </div>
         );
    }
}
 
export default Channels;