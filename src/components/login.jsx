import React, { Component } from "react";
import Cookies from "universal-cookie";
import $ from "jquery";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import {api_source} from "../App"


class Login extends Component {
  state = {
    email: null,
    password: null,
    login: false,
    store: null,
  };
  login(e) {
    e.preventDefault();
    var myForm = document.getElementById("form");
    var formData = new FormData(myForm);
    var jsonFormData = Object.fromEntries(formData);
    let cred_data = {
      user: jsonFormData,
    };
    console.log("form data:");
    console.log(cred_data);
    $.ajax({
      type: "POST",
      url: `${api_source}/users/login`,
      crossDomain: true,
      data: cred_data,
      dataType: "json",
    })
      .done(function (data) {
        console.log(data);
        var username = data["username"];
        var email = data["email"];
        let cookie = new Cookies();
        cookie.set("btoken", data.user.token);
        console.log(cookie.get("btoken"));
      })
      .fail(function (jqXHR, status, error) {
        console.log(jqXHR);
        // alert(status);
        console.log(error);
        console.log("hello");
      });
      this.storeCollector();
  }
  componentDidMount() {
    this.storeCollector();
  }
  storeCollector() {
    let cookies = new Cookies(document.cookie);
    let store = cookies.get("btoken");
    console.log("store",store)
    if (store) {
      this.setState({login: true, store: store});
    }
    console.log("after-setting-state",this.state)
  }
  render() {
    console.log(this.state)
    let loginjsx = (
        <div className="container container-border login">
        <div className="row row-header">
          <header>
            <h1>Login</h1>
          </header>
        </div>
        <div className="row row-extra">
          <form id="form">
            <div className="form-group">
              <label>
                Email address
                <input
                  type="email"
                  className="form-control"
                  id="exampleInputEmail1"
                  name="email"
                  aria-describedby="emailHelp"
                />
              </label>
              <small id="emailHelp" className="form-text text-muted">
                We'll never share your email with anyone else.
              </small>
            </div>
            <div className="form-group">
              <label>
                password
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  name="password"
                />
              </label>
            </div>
            <button
              type="submit"
              id="submit"
              className="btn btn-primary"
              onClick={(event) => {
                this.login(event);
              }}
            >
              Submit
            </button>
          </form>
        </div>
        <a href="/signup" class="badge badge-light">Signup</a>
      </div>
    );
    let redirect = <Redirect to="/" />
    return (
        this.state.login?redirect:loginjsx
    );
  }
}

export default Login;
