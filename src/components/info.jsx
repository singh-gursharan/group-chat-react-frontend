import React, { Component } from "react";
import User from "./user";


class Info extends Component {
  state = {};

  render() {
    const users = this.props.info.users.map((user) => {
      return <User key={user.id} user={user} />;
    });
    const is_admin =
      this.props.currentChannel.adminId === this.props.info.currentUserId;
    console.log("is_admin", is_admin);
    var infoJsx = <React.Fragment>{users}</React.Fragment>;
    if (is_admin) {
      infoJsx = (
        <React.Fragment>
          <div className="list-group">
            <button
              type="button"
              className="list-group-item list-group-item-action active"
              onClick={(e) => {
                console.log("showAddChannelModal called")
                this.props.showAddUserModal();
              }}
            >
              Add user
            </button>
            {users}
          </div>
        </React.Fragment>
      );
    }
    return infoJsx;
  }
}

export default Info;
