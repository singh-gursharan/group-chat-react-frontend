import React, { Component } from "react";
import Cookies from "universal-cookie";
import $, { get } from "jquery";
import ChatBox from "./chatbox";
import Channels from "./channels";
import Info from "./info";
import AddChannelModal from "./add_channel_modal";
import AddUserModal from "./add_user_modal";
import {api_source} from "../App"


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";

class Home extends Component {
  state = {
    addUserModal: {
      show: false,
    },
    addChannelModal: {
      show: false,
    },
    token: null,
    authorized: true,
    userId: null,
    userName: null,
    channels: [],
    current_channel: null,
    chatbox: {
      show: false,
      channel: { name: null, id: null, adminId: null },
      posts: [],
    },
    info: {
      show: false,
      users: [],
      currentUserId: null,
      channel: null,
    },
  };
  componentDidMount() {
    let cookies = new Cookies(document.cookie);
    let token = cookies.get("btoken");
    var authorized = true;
    var channels = this.state.channels;
    var userId = this.state.userId;
    var userName = this.state.userName;
    console.warn("api source", api_source)
    var setState = (obj) => {
      this.setState(obj);
    };
    $.ajax({
      type: "GET",
      url: `${api_source}/current/user`,
      crossDomain: true,
      headers: { Authorization: `Bearer ${token}` },
      dataType: "json",
    })
      .done(function (data) {
        console.log(data);
        userId = data.user.id;
        userName = data.user.username;
        $.ajax({
          type: "GET",
          async: false,
          url: `${api_source}/users/1/channels`,
          crossDomain: true,
          headers: { Authorization: `Bearer ${token}` },
          dataType: "json",
        }).done(function (data) {
          console.log(data);
          channels = data.channels.map((channel) => {
            return {
              id: channel.id,
              name: channel.name,
              adminId: channel.admin.id,
            };
          });
        });
      })
      .fail(function (jqXHR, status, error) {
        console.log(jqXHR);
        alert(status);
        console.log(error);
        console.log("hello");
        authorized = false;
      })
      .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {
        setState({
          token: token,
          userId: userId,
          userName: userName,
          authorized: authorized,
          channels: channels,
        });
      });
    console.log("after request");
    // this.setState({
    //   userId: userId,
    //   userName: userName,
    //   authorized: authorized,
    //   channels: channels,
    // });
  }
  onChannelSelect = (e, channel) => {
    var posts = [];
    if (this.state.current_channel == channel) {
      posts = this.state.chatbox.posts;
    }
    const cb = {
      show: true,
      channel: { name: channel.name, id: channel.id, adminId: channel.adminId },
      posts: posts,
    };
    const info = {
      show: false,
      users: [],
      currentUserId: this.state.userId,
      channel: this.state.info.channel,
    };
    this.setState({
      chatbox: cb,
      current_channel: channel,
      info: info,
    });
    // Uncomment the below code when endpoint is created on the backend server.
    // $.ajax({
    //     type: GET,
    //     url: `${api_source}/users/1/channels/${channel.id}/posts`,
    //     dataType: "json",
    //   }).done(function (data) {
    //     console.log(`posts returned by ${channel.name}`, data);
    //     const cb = {
    //       show: true,
    //       channel: {
    //         name: channel.name,
    //         id: channel.id,
    //         adminId: channel.adminId,
    //       },
    //       posts: data.posts,
    //     };
    //     this.setState({
    //       chatbox: cb,
    //     });
    //   });
  };
  onClickChatBoxHead = (e, channel) => {
    //this channel argument may
    // be removed in future as it is nothing but current
    // selected channel, so we can also put the current
    // _channel from state and get the value from state.
    const currentUserId = this.state.userId;
    const show = this.state.info.show;
    var setState = (obj) => {
      console.log("setState called");
      this.setState(obj);
    };
    $.ajax({
      type: "GET",
      headers: { Authorization: `Bearer ${this.state.token}` },
      url: `${api_source}/users/${this.state.userId}/channels/${channel.id}/users`,
      dataType: "json",
    }).done(function (data) {
      console.log(`list of user in the channel name ${channel.name}`);
      let newInfo = {
        show: !show,
        users: data.users,
        currentUserId: currentUserId,
        channel: channel,
      };
      setState({ info: newInfo });
    });
  };
  addPostHandler = (post) => {
    const cb = {
      show: true,
      channel: {
        name: this.state.chatbox.channel.name,
        id: this.state.chatbox.channel.id,
        adminId: this.state.chatbox.channel.adminId,
      },
      posts: this.state.chatbox.posts.concat([...post]),
    };

    this.setState({ chatbox: cb });
  };
  showAddUserModal = () => {
    this.setState({ addUserModal: { show: true } });
  };
  hideAddUserModal = () => {
    this.setState({ addUserModal: { show: false } });
  };
  showAddChannelModal = () => {
    this.setState({ addChannelModal: { show: true } });
  };
  hideAddChannelModal = () => {
    this.setState({ addChannelModal: { show: false } });
  };
  addUserToNewChannelHandler = (newChannel) => {
    this.hideAddChannelModal();
    const channels = [...this.state.channels, newChannel];
    this.setState({
      addUserModal: {
        show: true,
      },
      addChannelModal: {
        show: false,
      },
      chatbox: {show: true, channel: newChannel, posts: []},
      channels: channels,
      current_channel: newChannel,
      info: {
        show: false,
        users: [newChannel.admin],
        currentUserId: this.state.info.currentUserId,
        channel: newChannel,
      },
    });
  };
  logoutHandler = () => {
    
    const currentUserId = this.state.userId;
    const token = this.state.token;
    const setState = (obj) => {
      this.setState(obj);
    };
    $.ajax({
      type: "GET",
      url: `${api_source}/users/${currentUserId}/logout`,
      crossDomain: true,
      headers: { Authorization: `Bearer ${token}` },
      dataType: "json",
    })
      .done(function (data) {
        let cookies = new Cookies(document.cookie);
        let cookie = cookies.remove("btoken");
        console.log("logout status", data);
        setState({ authorized: false, token: null });
      })
      .fail(function (jqXHR, status, error) {
        console.log(jqXHR);
        alert(status);
        console.log(error);
        console.log("logout fail");
      });
  };
  render() {
    // if(this.state.current_channel && !this.state.current_channel.id==this.state.chatbox.channel.id)
    //   this.setState({chatbox:{show: this.state.chatbox.show, channel: this.state.current_channel, posts: this.state.chatbox.posts}})
    console.log(this.state.chatbox.posts);
    console.log("chatbox show: ", this.state.chatbox.show);
    const logout = (
      <button
        type="button"
        class="btn btn-default btn-sm"
        onClick={(e) => {
          this.logoutHandler();
        }}
      >
        <span class="glyphicon glyphicon-log-out"></span> Log out
      </button>
    );
    var addUserModal = null;
    if (this.state.addUserModal.show) {
      addUserModal = (
        <AddUserModal
          currentChannel={this.state.current_channel}
          token={this.state.token}
          handleCloseAddUserModal={this.hideAddUserModal}
          currentUserId={this.state.userId}
          addedUsers={this.state.info.users}
        />
      );
    }
    var addChannelModal = null;
    if (this.state.addChannelModal.show) {
      addChannelModal = (
        <AddChannelModal
          token={this.state.token}
          handleCloseAddChannelModal={this.hideAddChannelModal}
          currentUserId={this.state.userId}
          addUserToNewChannelHandler={this.addUserToNewChannelHandler}
        />
      );
    }

    const user = (
      <div className="user">
        <div>
          <h2>{this.state.userName}</h2>
        </div>
        <div>
          <button
            type="button"
            class="btn btn-light"
            onClick={(e) => {
              this.showAddChannelModal();
            }}
          >
            Add Channel
          </button>
        </div>
      </div>
    );

    var homeJsx = (
      <div className="container-fluid container-extra">
        <div className="row container-extra">
          <div className="col-sm-3 channels-extra">
            {logout}
            {user}
            <Channels
              currentChannel={this.state.current_channel}
              channels={this.state.channels}
              onChannelSelect={this.onChannelSelect}
            />
          </div>
          <div className="col chat-image">
            <img src="https://i.pinimg.com/originals/15/0d/f7/150df7d06eeda9a9d90936b29156c37c.png" />
          </div>
          {addChannelModal}
          {addUserModal}
        </div>
      </div>
    );
    if (this.state.chatbox.show) {
      homeJsx = (
        <div className="container-fluid container-extra">
          <div className="row container-extra">
            <div className="col-sm-3 channels-extra">
            {logout}
              {user}
              <Channels
                channels={this.state.channels}
                currentChannel={this.state.current_channel}
                onChannelSelect={this.onChannelSelect}
              />
            </div>
            <div className="col d-flex flex-column bd-highlight chatbox">
              <ChatBox
                chatbox={this.state.chatbox}
                currentUserId={this.state.userId}
                addPostHandler={this.addPostHandler}
                onClickChatBoxHead={this.onClickChatBoxHead}
              />
            </div>
            {addChannelModal}
            {addUserModal}
          </div>
        </div>
      );
      if (this.state.info.show) {
        console.log("inside if");
        homeJsx = (
          <div className="container-fluid container-extra">
            <div className="row container-extra">
              <div className="col-sm-3 channels-extra">
              {logout}
                {user}
                <Channels
                  currentChannel={this.state.current_channel}
                  channels={this.state.channels}
                  onChannelSelect={this.onChannelSelect}
                />
              </div>
              <div className="col d-flex flex-column bd-highlight chatbox">
                <ChatBox
                  chatbox={this.state.chatbox}
                  currentUserId={this.state.userId}
                  addPostHandler={this.addPostHandler}
                  onClickChatBoxHead={this.onClickChatBoxHead}
                />
              </div>
              <div className="col">
                <Info
                  currentChannel={this.state.current_channel}
                  showAddUserModal={this.showAddUserModal}
                  info={this.state.info}

                />
              </div>
            </div>
            {addChannelModal}
            {addUserModal}
          </div>
        );
      }
    }

    let redirect = <Redirect to="/login" />;
    return this.state.authorized ? homeJsx : redirect;
  }
}

export default Home;
