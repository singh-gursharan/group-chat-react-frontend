import React, { Component } from "react";
import $ from "jquery";
import {api_source} from "../App"


class AddChannelModal extends Component {
  state = {
    users: [],
  };

  componentDidMount() {
    // const addedUserids = this.props.addedUsers.map((e) => {
    //   return e.id;
    // });
    const setState = (obj) => {
      this.setState(obj);
    };
    $.ajax({
      type: "GET",
      headers: { Authorization: `Bearer ${this.props.token}` },
      url: `${api_source}/users`,
      dataType: "json",
    }).done(function (data) {
      console.log(`list of all user`, data);
      let users = data.users;
      // users = users.filter((e) => {
      //   return !addedUserids.includes(e.id);
      // });
      const state = { users: users };
      setState(state);
    });
  }
  addChannelHandler = (e, addUserToNewChannelHandler) => {
    e.preventDefault();
    // var users = this.state.users;
    const setState = (obj) => {
      this.setState(obj);
    };
    var form = $("#add-channel-form");
    var formdata = form.serializeArray();
    console.log(formdata);
    formdata = formdata.map((ele, index) => ele.value);
    console.log(formdata);
    var data = {
      channel: {
        name: formdata[0],
      },
    };
    $.ajax({
      type: "POST",
      crossDomain: true,
      headers: { Authorization: `Bearer ${this.props.token}` },
      url: `${api_source}/users/${this.props.currentUserId}/channels`,
      dataType: "json",
      data: data,
    }).done(function (data) {
      console.warn(`channel has been created`, data);
      const newChannel = {
        name: data.channel.name,
        id: data.channel.id,
        adminId: data.channel.admin.id,
        admin: data.channel.admin
      };
      addUserToNewChannelHandler(newChannel);
    });
  };
  render() {
    let listUserTag = this.state.users.map((user) => {
      return (
        <div key={user.id} className="form-group form-check">
          <input
            className="form-check-input"
            type="checkbox"
            id={user.id}
            name={user.username}
            value={user.id}
          />
          <label className="form-group form-check" for={user.username}>
            {" "}
            {user.username}
          </label>
        </div>
      );
    });
    return (
      <div className="modal">
        <div className="modal-main">
          <button
            type="button"
            class="close"
            aria-label="Close"
            onClick={(e) => {
              this.props.handleCloseAddChannelModal();
            }}
          >
            <span aria-hidden="true">&times;</span>
          </button>

          <form method="POST" id="add-channel-form">
            <div class="form-group">
              <label for="channel">Add channel</label>
              <input
                className="form-control"
                id="name"
                name="name"
                placeholder="channel name"
              />
            </div>
            <input
              className="btn btn-primary"
              type="submit"
              onClick={(e) => {
                this.addChannelHandler(
                  e,
                  this.props.addUserToNewChannelHandler
                );
              }}
              value="Submit"
            />
          </form>
        </div>
      </div>
    );
  }
}

export default AddChannelModal;
