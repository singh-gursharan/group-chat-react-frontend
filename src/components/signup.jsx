import React, { Component } from "react";
import $ from "jquery";
import Cookies from "universal-cookie";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import {api_source} from "../App"


class SignUp extends Component {
  state = {
    signup: false,
  };
  signup(e) {
    e.preventDefault();
    var myForm = document.getElementById("form");
    var formData = new FormData(myForm);
    var jsonFormData = Object.fromEntries(formData);
    const setState = (obj) => this.setState(obj);
    let cred_data = {
      user: jsonFormData,
    };
    console.log("form data:");
    console.log(cred_data);
    $.ajax({
      type: "POST",
      url: `${api_source}/users`,
      crossDomain: true,
      data: cred_data,
      dataType: "json",
    })
      .done(function (data) {
        console.log(data);
        var username = data["username"];
        var email = data["email"];
        console.log("data", email);
        let cookie = new Cookies();
        cookie.set("btoken", data.user.token);
        console.log(cookie.get("btoken"));
        setState({ signup: true });
      })
      .fail(function (jqXHR, status, error) {
        console.log(jqXHR);
        alert(status);
        console.log(error);
        console.log("hello");
      });
  }
  render() {
    var signup = (
      <div className="container container-border">
        <div className="row row-header">
          <header>
            <h1>Signup</h1>
          </header>
        </div>
        <div className="row row-extra">
          <form id="form">
            <div className="form-group">
              <label>
                User name
                <input
                  type="name"
                  className="form-control"
                  id="username"
                  name="username"
                />
              </label>
            </div>
            <div className="form-group">
              <label>
                Email address
                <input
                  type="email"
                  className="form-control"
                  id="exampleInputEmail1"
                  name="email"
                  aria-describedby="emailHelp"
                />
              </label>
              <small id="emailHelp" className="form-text text-muted">
                We'll never share your email with anyone else.
              </small>
            </div>
            <div className="form-group">
              <label>
                password
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  name="password"
                />
              </label>
            </div>
            <button
              id="submit"
              className="btn btn-primary"
              onClick={(event) => {
                this.signup(event);
              }}
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    );
    const redirect = <Redirect to="/" /> 
    return this.state.signup? redirect: signup;
  }
}

export default SignUp;
