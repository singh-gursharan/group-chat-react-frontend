import React, { Component } from "react";

class Channel extends Component {
  state = {};

  render() {
    var channel = null
    if (this.props.currentChannel!=null && (this.props.currentChannel.id === this.props.channel.id)) {
      channel = (
        <button
          type="button"
          className="list-group-item list-group-item-action active"
          onClick={(e) => {
            this.props.onChannelSelect(e, this.props.channel);
          }}
        >
          {this.props.channel.name}
        </button>
      );
    } else {
      channel = (
        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={(e) => {
            this.props.onChannelSelect(e, this.props.channel);
          }}
        >
          {this.props.channel.name}
        </button>
      );
    }
    return channel;
  }
}

export default Channel;
