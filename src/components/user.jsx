import React, { Component } from "react";

class User extends Component {
  state = {};
  render() {
    return (
      <button
        type="button"
        className="list-group-item list-group-item-action"
      >
        {this.props.user.username}
      </button>
    );
  }
}

export default User;
