import React, { Component } from "react";
import $ from "jquery";
import {api_source} from "../App"


class AddUserModal extends Component {
  state = {
    users: [],
  };

  componentDidMount() {
    const addedUserids = this.props.addedUsers.map((e) => {
      return e.id;
    });
    const setState = (obj) => {
      this.setState(obj);
    };
    $.ajax({
      type: "GET",
      headers: { Authorization: `Bearer ${this.props.token}` },
      url: `${api_source}/users`,
      dataType: "json",
    }).done(function (data) {
      console.log(`list of all user`, data);
      let users = data.users;
      users = users.filter((e) => {
        return !addedUserids.includes(e.id);
      });
      const state = { users: users };
      setState(state);
    });
  }
  addUserHandler = (e) => {
    e.preventDefault();
    var users = this.state.users;
    const setState = (obj) => {
      this.setState(obj);
    };
    var form = $("#add-user-form");
    var formdata = form.serializeArray();
    console.log(formdata);
    formdata = formdata.map((ele) => ele.value);
    console.log(formdata);
    var data = {
      user_channel: {
        channel_id: this.props.currentChannel.id,
        user_ids: formdata,
      },
    };
    $.ajax({
      type: "POST",
      crossDomain: true,
      headers: { Authorization: `Bearer ${this.props.token}` },
      url: `${api_source}/users/${this.props.currentUserId}/channels/${this.props.currentChannel.id}/multi_users`,
      dataType: "json",
      data: data,
    }).done(function (data) {
      console.log(`user has been inserted`, data);
      users = users.filter((user) => {
        return !data.user_ids.includes(user.id);
      });
      const state = { users: users };
      setState(state);
    });
  };
  render() {
    let listUserTag = this.state.users.map((user) => {
      return (
        <div key={user.id} className="form-group form-check">
          <input
            className="form-check-input"
            type="checkbox"
            id={user.id}
            name={user.username}
            value={user.id}
          />
          <label className="form-group form-check" for={user.username}>
            {" "}
            {user.username}
          </label>
        </div>
      );
    });
    return (
      <div className="modal">
        <div className="modal-main">
          <button type="button" class="close" aria-label="Close" onClick = {e => {this.props.handleCloseAddUserModal()}}>
            <span aria-hidden="true">&times;</span>
          </button>
          <header>
            <h2>Add user</h2>
          </header>
          <form method="POST" id="add-user-form">
            <div class= "user-list">
            {listUserTag}
            </div>
            <input
              className="btn btn-primary"
              type="submit"
              onClick={(e) => {
                this.addUserHandler(e);
              }}
              value="Submit"
            />
          </form>
        </div>
      </div>
    );
  }
}

export default AddUserModal;
