import React, { Component } from "react";

class Post extends Component {
  state = {};
  render() {
  var postjsx = <div class="other-post-row"><div class="post-name">{this.props.post.user.username}</div><div>{this.props.post.message}</div></div>;

    if (this.props.currentUserId == this.props.post.user.id) {
      postjsx = <div class="my-post-row"><div class="my-post post-name">{this.props.post.user.username}</div><div class="my-post">{this.props.post.message}</div></div>;
    }
    return postjsx;
  }
}

export default Post;
