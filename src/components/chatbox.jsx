import React, { Component } from "react";
import $ from "jquery";
import Post from "./post";
import { Socket } from "phoenix";
import Cookies from "universal-cookie";
import {ws_source} from "../App"

class ChatBox extends Component {
  state = {
    channelSocket: null,
    channel: null,
  };
  sendPost = (e, addPostHandler) => {
    var post = $("#post-input").val();
    // Add the code here for listen on channel using phoenix js library
    // and send the data returned on addPostHandler which will
    // inturn change the state of the chatbox by adding post to posts value
    // addPostHandler(post)
  };
  // static getDerivedStateFromProps(props, state) {
  //   if (state.channel != props.chatbox.channel) {
  //     let postsHandler = (posts) => {
  //       props.addPostHandler(posts);
  //     };
  //     let cookies = new Cookies(document.cookie);
  //     let token = cookies.get("btoken");
  //     let socket = new Socket("${ws_source}/socket", {
  //       params: { token: token },
  //     });
  //     let channel = socket.channel(`channel:${props.chatbox.channel.id}`, {});
  //     channel.join().receive("ok", (response) => {
  //       console.log("Joined successfully", response);
  //       postsHandler(response.posts);
  //     });
  //     this.setState({ channelSocket: channel });
  //     socket.connect();
  //     channel.on("response:updated", (response) => {
  //       const post = [response.post]; //enclosed in list because we already have a method(postsHandler)
  //       //in home that loads all the posts when we join the channel(as above).
  //       postsHandler(post);
  //     });
  //   }
  //   console.log("getDerivedStateFromProps");
  //   return { channel: props.chatbox.channel };
  // }
  static getDerivedStateFromProps(props, state) {
    return {channel: props.chatbox.channel };
  }
  componentDidMount() {
    console.log("mounted");
    let postsHandler = (posts) => {
      this.props.addPostHandler(posts);
    };
    let cookies = new Cookies(document.cookie);
    let token = cookies.get("btoken");
    let socket = new Socket(`${ws_source}/socket`, {
      params: { token: token },
    });
    let channel = socket.channel(
      `channel:${this.props.chatbox.channel.id}`,
      {}
    );
    channel.join().receive("ok", (response) => {
      console.log("Joined successfully", response);
      postsHandler(response.posts);
    });
    this.setState({ channelSocket: channel });
    socket.connect();
    channel.on("response:updated", (response) => {
      const post = [response.post]; //enclosed in list because we already have a method(postsHandler)
      //in home that loads all the posts when we join the channel(as above).
      postsHandler(post);
    });
    console.log("component did mount");
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.channel.id != this.state.channel.id) {
      prevState.channelSocket.leave()
      console.log("preState!=currnetState")
          let postsHandler = (posts) => {
            this.props.addPostHandler(posts);
          };
          let cookies = new Cookies(document.cookie);
          let token = cookies.get("btoken");
          let socket = new Socket(`${ws_source}/socket`, {
            params: { token: token },
          });
          let channel = socket.channel(`channel:${this.props.chatbox.channel.id}`, {});
          channel.join().receive("ok", (response) => {
            console.log("Joined successfully", response);
            postsHandler(response.posts);
          });
          this.setState({ channelSocket: channel });
          socket.connect();
          channel.on("response:updated", (response) => {
            const post = [response.post]; //enclosed in list because we already have a method(postsHandler)
            //in home that loads all the posts when we join the channel(as above).
            postsHandler(post);
          });
        }
    console.log("component did update");
  }
  onSend_handler = function (e) {
    var post = $("#post-input").val();
    console.log(`ready to push value ${post}`);
    this.state.channelSocket.push("response:update", { response: post });
  };
  render() {
    let listposts = this.props.chatbox.posts.map((post, index) => {
      console.log("index", index);
      console.log("post", post);
      return (
        <Post
          key={post.id}
          currentUserId={this.props.currentUserId}
          post={post}
        />
      );
    });
    console.log("listpost", listposts)
    console.log("this.chatbox.posts", this.props.chatbox.posts);
    listposts.sort((a,b) => {
      return (a.key - b.key)
    })
    return (
      <React.Fragment>
        <div
          className="row p-2 bd-highlight chatbox-head"
          onClick={(e) => {
            this.props.onClickChatBoxHead(e, this.props.chatbox.channel);
          }}
        ><h2>{this.props.chatbox.channel.name}</h2>
        </div>
        <div className="row p-2 flex-grow-1 bd-highlight posts-wrap">
          <div class="posts-flex">{listposts}</div>
        </div>
        <div className="row p-2 bd-highlight">
          <div className="input-group mb-3">
            <input
              id="post-input"
              type="text"
              className="form-control"
              placeholder="Write some message to deliver"
              aria-label=""
              aria-describedby="basic-addon2"
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={(e) => {
                  // this.sendPost(e, this.props.addPostHandler();
                  this.onSend_handler(e);
                }}
              >
                Send
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ChatBox;
