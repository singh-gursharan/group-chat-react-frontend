import React from "react";
import "./App.css";
import SignUp from "./components/signup";
import "bootstrap/dist/css/bootstrap.css";
import "./home.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import Login from "./components/login";
import Home from "./components/home";

function App() {
  return (
    <Router>
      <Switch>
        <Route path={"/login"} component={Login} />{" "}
        <Route path={"/signup"} component={SignUp} />{" "}
        <Route path={"/"} component={Home} />{" "}
      </Switch>{" "}
    </Router>
  );
}

//export const api_source = "http://localhost:4000/api";
//export const ws_source = "ws://localhost:4000";
export const api_source = "https://infinite-taiga-40727.herokuapp.com/api";
export const ws_source = "wss://infinite-taiga-40727.herokuapp.com";
export default App;
